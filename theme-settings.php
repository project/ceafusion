<?php

function ceafusion_form_system_theme_settings_alter(&$form, $form_state){
  $form['tnt_container']['general_settings']['ceafusion_custom_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cea Fusion Settings'),
    '#description' => t("These settings control some aspects from Cea Fusion."),
    '#weight' => -18,
  );

  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_remove_name_on_submitted_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove name on submitted info'),
    '#default_value' => theme_get_setting('ceafusion_remove_name_on_submitted_info'),
    '#prefix' => '<style>table *{vertical-align:top;} </style><table><tr><td>',
  );
    
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_qr_code'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display QR Code'),
    '#default_value' => theme_get_setting('ceafusion_qr_code'),
  );

  $options = array( 1 => t('Without align'), 2 => t('Left align'), 3=> t('Right align')  );
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_image_align'] = array(
    '#type' => 'radios',
    '#title' => t('Image align in front page'),
    '#default_value' => theme_get_setting('ceafusion_image_align') ? theme_get_setting('ceafusion_image_align') : 1,
    '#options' => $options,
  );
  
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_social_area'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display area whit social links'),
    '#default_value' => theme_get_setting('ceafusion_social_area'),
    '#prefix' => '</td><td>'
  );
  
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_twitter_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter id'),
    '#maxlength'=> 40,
    '#size' => 20,
    '#default_value' => theme_get_setting('ceafusion_twitter_id'),
    '#states' => array(
      'invisible' => array(
         ':input[name="ceafusion_social_area"]' => array('checked' => FALSE),
        ),
    ),
  );
  
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_twitter_widget'] = array(
    '#type' => 'checkbox',
    '#title' => t('Twitter widget'),
    '#default_value' => theme_get_setting('ceafusion_twitter_widget'),
    '#states' => array(
      'invisible' => array(
         ':input[name="ceafusion_twitter_id"]' => array('filled' => FALSE),
        ),
    ),
  );
  
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_facebook_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook id'),
    '#maxlength'=> 40,
    '#size' => 20,
    '#default_value' => theme_get_setting('ceafusion_facebook_id'),
    '#states' => array(
      'invisible' => array(
         ':input[name="ceafusion_social_area"]' => array('checked' => FALSE),
        ),
    ),
    '#suffix' => '</td><td>',
  );
  
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_make_metatags'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add metatags'),
    '#default_value' => theme_get_setting('ceafusion_make_metatags') ? theme_get_setting('ceafusion_make_metatags'): 1,
    '#description' => t('Add metatags whit page title, author, date, image...'),
  );
  
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_metatags_str'] = array(
    '#type' => 'textfield',
    '#title' => t('Add this text to metatags'),
    '#default_value' => theme_get_setting('ceafusion_metatags_str') ? theme_get_setting('ceafusion_metatags_str'): '',
    '#description' => t('Add this text to standard metatags'),
    '#suffix' => '</td></tr></table>',
  );
  
  
  
  drupal_add_css('misc/farbtastic/farbtastic.css');
  drupal_add_js('misc/farbtastic/farbtastic.js');  

  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Color'),
    '#default_value' => theme_get_setting('ceafusion_background_color') ? theme_get_setting('ceafusion_background_color') : '#ffffff',
    '#description' => '<div id="background_color_colorpicker"></div>',
    '#size' => 9,
    '#maxlength' => 7,
    '#prefix' =>'<table id="colors_container"><tr><th>'.t('Page').'</th><th>'.t('Blocks').'</th><th>'.t('Links and borders').'</th></tr><tr><td>',
    '#suffix' => "<script type='text/javascript'>
    jQuery(document).ready(function() {
      jQuery('#background_color_colorpicker').farbtastic('#edit-ceafusion-background-color');
    });
    </script>",
  );   
  
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_background_blocks'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Color'),
    '#default_value' => theme_get_setting('ceafusion_background_blocks') ? theme_get_setting('ceafusion_background_blocks') : '#ffffff',
    '#description' => '<div id="background_blocks_colorpicker"></div>',
    '#size' => 9,
    '#maxlength' => 7,
    '#prefix' => '</td><td>',
    '#suffix' => "<script type='text/javascript'>
    jQuery(document).ready(function() {
      jQuery('#background_blocks_colorpicker').farbtastic('#edit-ceafusion-background-blocks');
    });
    </script>",
  );   

  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_links_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Links and borders color'),
    '#default_value' => theme_get_setting('ceafusion_links_color') ? theme_get_setting('ceafusion_links_color') : '#000000',
    '#description' => '<div id="links_color_colorpicker"></div>',
    '#size' => 9,
    '#maxlength' => 7,
    '#states' => array(
      'invisible' => array(
         ':input[name="ceafusion_modify_links_colors"]' => array('checked' => FALSE),
        ),
    ),
    '#suffix' => "<script type='text/javascript'>
    jQuery(document).ready(function() {
      jQuery('#links_color_colorpicker').farbtastic('#edit-ceafusion-links-color');
    });
    </script></td></tr>",
    '#prefix' => '</td><td>',
  );
  
  $options = array();
  for ( $i=0;$i<11;$i++ ){
    $options[$i+1] = ($i*10).'%';
  }

  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_page_opacity'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Opacity'),
    '#default_value' => theme_get_setting('ceafusion_page_opacity') ? theme_get_setting('ceafusion_page_opacity'): 11,
    '#prefix' => '<tr><td>',
    '#suffix' => '</td><td>'
  );
 
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_blocks_opacity'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Opacity'),
    '#default_value' => theme_get_setting('ceafusion_blocks_opacity') ? theme_get_setting('ceafusion_blocks_opacity'): 11,
  ); 
  
  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_borders_in_main_area'] = array(
    '#type' => 'checkbox',
    '#title' => t('Borders in main area'),
    '#default_value' => theme_get_setting('ceafusion_borders_in_main_area') ? theme_get_setting('ceafusion_borders_in_main_area'): 0,
    '#states' => array(
      'invisible' => array(
         ':input[name="ceafusion_modify_links_colors"]' => array('checked' => FALSE),
        ),
    ),
    '#prefix' => '</td><td>',
    '#suffix' => "</td></tr></table>",
  );  
  

  $form['tnt_container']['general_settings']['ceafusion_custom_fields']['ceafusion_date_of_setting'] = array('#type' => 'hidden', '#value' => time());
}


