<?php

function ceafusion_preprocess_node(&$vars){
  if ( theme_get_setting('ceafusion_remove_name_on_submitted_info') ){
    $vars['submitted'] =  t('Submitted on !datetime', 
		  array(
		  '!datetime' => $vars['date'],
		  )
		);
  }
}


function ceafusion_preprocess_page(&$vars){
  global $base_url;
	$vars['ceafusion_display_QR'] = theme_get_setting('ceafusion_qr_code');
	$vars['ceafusion_QR_code'] = "<img src=\"http://chart.apis.google.com/chart?chs=80x80&cht=qr&chl=" .$base_url.'/'.current_path(). "\" width=\"120\" height=\"120\" alt=\"\" />";
	
	$vars['ceafusion_display_social_area'] = theme_get_setting('ceafusion_social_area');
	
	if ( $vars['ceafusion_display_social_area'] ){
	  $social_area = '<div id="ceafusion_social_links">';
	
	  if ( theme_get_setting('ceafusion_twitter_widget') ){
	    $social_area .= '
	    <script>
	      !function(d,s,id){
	        var js,fjs=d.getElementsByTagName(s)[0];
	        if(!d.getElementById(id)){
	          js=d.createElement(s);
	          js.id=id;
	          js.src="//platform.twitter.com/widgets.js";
	          fjs.parentNode.insertBefore(js,fjs);
          }
        }
        (document,"script","twitter-wjs");
      </script>
      <a href="https://twitter.com/'.theme_get_setting('ceafusion_twitter_id').'" 
          class="twitter-follow-button" data-show-count="true" data-lang="es"data-show-screen-name="false">
          '.t('Follow').' @'.theme_get_setting('ceafusion_twitter_id').
      '</a>'
      ;
	  }
	  
	  if ( theme_get_setting('ceafusion_twitter_id') ){
	    $img_tw = theme_image(array('path' => "/sites/all/themes/ceafusion/images/twitter_icon.png",'attributes' => array()));
	    $social_area .= l( $img_tw, 'http://twitter.com/'.theme_get_setting('ceafusion_twitter_id'), array('html' => true) );
	  }
	
	  if ( theme_get_setting('ceafusion_facebook_id') ){
	    $img_fb = theme_image(array('path' => "/sites/all/themes/ceafusion/images/facebook_icon.png",'attributes' => array()));
	    $social_area .= l( $img_fb, 'http://facebook.com/'.theme_get_setting('ceafusion_facebook_id'), array('html' => true) );
	  }

    $img_rss = theme_image(array('path' => "/sites/all/themes/ceafusion/images/rss_icon.png",'attributes' => array()));
    $social_area .= l( $img_rss, 'rss.xml', array('html' => true) );

	  $vars['ceafusion_social_area'] = $social_area.'</div>';  

	
	
	}
	ceafusion_metatags($vars);
  return $vars;
}


/**
 * Provide custom metatags
 */
function ceafusion_metatags($vars){
  global $base_url;
  global $language;
  $img = $vars['logo'];
  $author = '';
  $sitename = variable_get('site_name', '');
  $teaser = t('Article from ').'
             '.$sitename.'
             '.$vars['site_slogan'];
  $str_description = t('Article from ').$sitename.' | '.theme_get_setting('ceafusion_metatags_str');
  $title = $sitename;
  if (array_key_exists('node',$vars)){
    $node = $vars['node'];
    $title = $node->title .' | '.variable_get('site_name', '');
    if ( array_key_exists( LANGUAGE_NONE, $node->field_image )  ){
      $img = file_create_url($node->field_image['und'][0]['uri']);
    }
    $author = $node->name;
    $teaser = substr($node->body[LANGUAGE_NONE][0]['value'],0,600).'...';
  }
  elseif ( $vars['is_front'] ) {
    $title = t('Wellcome to ').' '.$title.'. '. $vars['site_slogan'];
  }
  if ( empty($author) ){
    $author = $sitename;
  }
  	  
	  
	  $tags = array(
		  'lang' => array(
		    '#tag' => 'meta',
		    '#attributes' => array(
		      'name' => 'lang',
		      'content' => $language
		    )
		  ),
		  'author' => array(
		    '#tag' => 'meta',
		    '#attributes' => array(
		      'name' => 'author',
		      'content' => $author
		    )
		  ),
		  'description' => array(
		    '#tag' => 'meta',
		    '#attributes' => array(
		      'name' => 'description',
		      'content' => $str_description,
		    )
		  ),
		  'keywords' => array(
		    '#tag' => 'meta',
		    '#attributes' => array(
		      'name' => 'keywords',
		      'content' => $str_description
		    )
		  ),
		  'DC.title' => array(
		    '#tag' => 'meta',
		    '#attributes' => array(
		      'name' => 'DC.title',
		      'content' => $title
		    )
		  ),
		  'DC.creator' => array(
			  '#tag' => 'meta',
			  '#attributes' => array(
				  'name' => 'DC.creator',
				  'content' => $author
			  )
		  ),
		  'DC.publisher' => array(
			  '#tag' => 'meta',
			  '#attributes' => array(
				  'name' => 'DC.publisher',
				  'content' => $author
			  )
		  ),
		  'DC.description' => array(
			  '#tag' => 'meta',
			  '#attributes' => array(
				  'name' => 'DC.description',
				  'lang' => 'es',
				  'content' => $teaser,
			  )
		  ),
		  'DC.subject' => array(
			  '#tag' => 'meta',
			  '#attributes' => array(
				  'name' => 'DC.subject',
				  'lang' => 'es',
				  'content' => $str_description
			  )
		  ),
		  'og:title' => array(
			  '#tag' => 'meta',
			  '#attributes' => array(
				  'name' => 'og:title',
				  'content' => $title,
			  )
		  ),
		  'og:type' => array(
			  '#tag' => 'meta',
			  '#attributes' => array(
				  'name' => 'og:type',
				  'content' => 'article',
			  )
		  ),
		  'og:description' => array(
			  '#tag' => 'meta',
			  '#attributes' => array(
				  'name' => 'og:description',
				  'content' => $str_description,
			  )
		  ),
		  'og:url' => array(
			  '#tag' => 'meta',
			  '#attributes' => array(
				  'name' => 'og:url',
				  'content' => $base_url.url( $_GET['q'] ),
			  )
		  ),
		  'og:site_name' => array(
			  '#tag' => 'meta',
			  '#attributes' => array(
				  'name' => 'og:site_name',
				  'content' => $sitename,
			  )
		  ),
	  );

    if ( !empty($img) ){
    $tags['og:image'] = array(
		  '#tag' => 'meta',
		  '#attributes' => array(
			  'name' => 'og:image',
			  'content' => $img,
		  )
	  );
    
    }

	  foreach ($tags as $key => $val) {
		  drupal_add_html_head($val, $key);
	  }
}

function html2rgb($color)
{
    if ($color[0] == '#')
        $color = substr($color, 1);

    if (strlen($color) == 6)
        list($r, $g, $b) = array($color[0].$color[1],
                                 $color[2].$color[3],
                                 $color[4].$color[5]);
    elseif (strlen($color) == 3)
        list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
    else
        return false;

    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);

    return "$r,$g,$b";
}

function ceafusion_load_custom_css(){
  $css_file = drupal_get_path('theme','ceafusion').'/schema/style.css';
  $time_css_file = fileatime($css_file);
  $time_theme_settings = theme_get_setting('ceafusion_date_of_setting');
  if ( $time_theme_settings > $time_css_file  ){
    $str_align = array( 
      1 => '', 
      2 => '.front .field-name-field-image {float:left; margin-right: 10px;}',
      3 => '.front .field-name-field-image {float:right; margin-left: 10px;}',
    );
    $align = theme_get_setting('ceafusion_image_align') ? theme_get_setting('ceafusion_image_align') : 1;
    $str_align = $str_align[$align];
    
    $page_opacity = theme_get_setting('ceafusion_page_opacity') ? (theme_get_setting('ceafusion_page_opacity') -1)/10 : 1;
    $blocks_opacity = theme_get_setting('ceafusion_blocks_opacity') ? (theme_get_setting('ceafusion_blocks_opacity') -1)/10 : 1;
    $border = theme_get_setting('ceafusion_borders_in_main_area')? theme_get_setting('ceafusion_borders_in_main_area'): 0;
    
    $bg_page_color  = theme_get_setting('ceafusion_background_color') ? theme_get_setting('ceafusion_background_color') : '#ffffff';
    $bg_blocks_color  = theme_get_setting('ceafusion_background_blocks') ? theme_get_setting('ceafusion_background_blocks') : '#ffffff';
    $links_color = theme_get_setting('ceafusion_links_color') ? theme_get_setting('ceafusion_links_color') : '#000000';
    
    $bg_page_color = html2rgb($bg_page_color);
    $bg_blocks_color = html2rgb($bg_blocks_color);
    $links_color = html2rgb($links_color);
    
    

    $bg_page_color_with_opacity = "rgba($bg_page_color,$page_opacity)";
    $bg_blocks_color_with_opacity = "rgba($bg_blocks_color,$blocks_opacity)";
    $bg_page_color = "rgb($bg_page_color)";
    $bg_blocks_color = "rgb($bg_blocks_color)";
    $links_color = "rgb($links_color)";
    
    $css_content = "
      $str_align
      #custom_background                          { background-color: $bg_page_color_with_opacity }
      .main-menu li:hover                         { background-color: $bg_blocks_color }
      .sidebar .block                             { background-color: $bg_blocks_color_with_opacity }
      .main-menu .block                           { background-color: $bg_blocks_color_with_opacity }
      .main-menu li:hover a                       { color: #ffffff; }
      .title, a                                   { color:            $links_color; }
      #header-group-inner, 
        #main-inner, 
        #footer-inner                             { background-color: #ffffff; }
      #ceafusion_social_links, .node-teaser       { border-bottom: 1px solid $links_color; }
      .sidebar .block, .main-menu .block          { border: 1px solid $links_color; }
      #main-menu-inner li                         { border-right: 1px solid $links_color; }
      #custom_border                              { border: {$border}px solid $links_color; }
      .shadow{
          -moz-box-shadow: 3px 3px 4px $links_color;
          -webkit-box-shadow: 3px 3px 4px $links_color;
          box-shadow: 3px 3px 4px $links_color; /* For IE 8 */
          -ms-filter: \"progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='$links_color')\"; /* For IE 5.5 - 7 */
          filter: progid:DXImageTransform.Microsoft.Shadow(Strength = 4, Direction = 135, Color = '$links_color');
      }
      tr.odd, tr.even                             { background-color: transparent; border-bottom: 1px solid $links_color}
      .pager li *                                  { border: $links_color 1px solid; }
    ";
    file_unmanaged_save_data( $css_content, $css_file, FILE_EXISTS_REPLACE );
  }
}
