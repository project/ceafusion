WHERE TO START
--------------

Welcome to CeaFusion! 


INSTALLATION
------------

 1. Download Cea Fusion from http://drupal.org/project/ceafusion

 2. Unpack the downloaded file, take the folders and place them in your
    Drupal installation under one of the following locations:
      sites/all/themes
        making it available to the default Drupal site and to all Drupal sites
        in a multi-site configuration
      sites/default/themes
        making it available to only the default Drupal site
      sites/example.com/themes
        making it available to only the example.com site if there is a
        sites/example.com/settings.php configuration file

    Note: you will need to create the "themes" folder under "sites/all/"
    or "sites/default/".

 2. Download Fusion Theme:
    Download from http://drupal.org/project/fusion
    
 3. Unpack the downloaded file and install (steep 2)
 
 4. Install the Fusion Accelerator module: 
    http://drupal.org/project/fusion_accelerator Fusion Accelerator makes Fusion even more powerful, giving you control over Fusion's 
    layout and style options in Drupal's interface. Download and install 
    this module like usual to get the most out of Fusion.

	* How to install modules: http://drupal.org/node/70151

 5. Configure your custom options.


FURTHER READING
---------------

Full documentation on using Fusion:
  http://fusiondrupalthemes.com/support/documentation

Full documentation on creating a custom Fusion subtheme:
  http://fusiondrupalthemes.com/support/theme-developers

Drupal theming documentation in the Theme Guide:
  http://drupal.org/theme-guide


