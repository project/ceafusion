(function ($) {
  Drupal.behaviors.ceafusion = {
    attach: function(context, settings) {
      $('#custom_border').corner(" tl tr bl br 14x" );
      $("#header-group-inner").corner("tl tr 14x"); 
      $("#footer-inner").corner("bl br 14x");
      $(".sidebar .block").corner("14x");
      
      var width = $("sidebar-second").width() - 12;
      $("#sidebar-second .block").width(width);
      
      var width = $("sidebar-first").width() - 12;
      $("#sidebar-first .block").width(width);

      $('#header-in-wrapper').removeClass('full-width');
      
      var classList =$('#header-in').attr('class').split(/\s+/);
      $.each( classList, function(index, item){
        if (item.substr(0,4) === 'grid') {
          $('#header-in').removeClass( item );
        }
      });
      $('#custom_border').width( $('#header-group-inner').width()  );
    }
  }
})(jQuery);


